from time import sleep


def test_math_should_work():
    sleep(2)
    assert 2 == 1 + 1


def test_math_should_fail():
    sleep(3)
    assert 4 == 2 * 3
